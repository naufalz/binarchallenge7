const router = require("express").Router()
const { Home, User, ApiController, AccountController } = require("../controller")
const Middleware = require("../middleware")

// Just API
///////////////////////////////////////////////////////////
//////// Untuk penggunaaan API baca pada README.md ////////
///////////////////////////////////////////////////////////
router.get("/api/data/user", ApiController.user_data)
router.post("/api/data/user/add", ApiController.user_register)
router.get("/api/data/user/:username", ApiController.user_data_one)
router.post("/api/data/user/:username/update", ApiController.user_data_update)
router.delete("/api/data/user/:username/delete", ApiController.user_data_delete)
router.post("/api/history/record", ApiController.record_history)

module.exports = router