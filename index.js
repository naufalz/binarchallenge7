const express = require('express')
const session = require('express-session');

const app = express()
app.use(session({
    secret: 'rahasia',
    resave: true,
    saveUninitialized: true
}));

const port = 3000
const routes = require("./routes")
const Middleware = require("./middleware/index")

const passport = require('./lib/passport')
app.use(passport.initialize())
app.use(passport.session())

app.use(express.static('views/assets'))
app.set("view engine", "ejs")

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use(routes)
app.use(Middleware.errorHandler)

app.all("*", (req, res) => {
    res.send("404 Page Not Found")
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
    console.log(`http://localhost:${port}`)
})