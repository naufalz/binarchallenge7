const models = require('../models');
const User = require('./UserController');
const UserGame = models.user_game;
const UserGameBiodata = models.user_game_biodata;
const UserGameHistory = models.user_game_history;

const moment = require("moment");

class AccountController {
    static history_delete(req, res, next) {
        const { history_id } = req.params
        const user_id = req.user.dataValues.id

        UserGameHistory.destroy({
            where: {
                id: history_id,
                user_game_id: user_id,
            }
        })
            .then((callback) => {
                if (callback) {
                    res.redirect('/account/history');
                } else {
                    res.status(200).json({
                        success: false,
                        message: 'User tidak ditemukan',
                    })
                }
            })
            .catch(err => {
                res.status(200).json({
                    success: false,
                    error: err,
                })
            });
    }

    static history(req, res, next) {
        const user_id = req.user.dataValues.id
        const name = req.user.dataValues.username

        UserGameHistory.findAll({
            where: {
                user_game_id: user_id,
            },
            order: [
                ['createdAt', 'DESC'],
            ],
            limit: 20,
            offset: 0,
        })
            .then(game_history => {
                const data = {
                    data: game_history,
                    user_id: user_id,
                    name: name,
                    moment: moment
                }
                res.render("account/history", data)

            })
            .catch(err => {
                res.status(200).json({
                    success: false,
                    error: err,
                })
            });
    }

    static account_edit(req, res, next) {
        const name = req.user.dataValues.username
        UserGameBiodata.findOne({
            where: {
                user_game_id: req.user.dataValues.id,
            }
        }).then(cb => {
            const data = {
                name: cb.name
            }
            res.render("account/edit", data)
        })
    }
    static account_edit_post(req, res, next) {
        const { name } = req.body
        const user_id = req.user.dataValues.id
        UserGameBiodata.update(
            {
                name: name
            },
            {
                where: { user_game_id: user_id }
            }
        ).then(callback => {
            if (callback) {
                req.user.dataValues.username = name;
                res.redirect('/');
            } else {
                res.redirect('/');
            }
        })

    }


}


module.exports = AccountController